#~~syntax:makefile
#==============================================================================
# tmctl - Conrad Telemetrics Unit data capturing and analyzation tools
#   Copyright © Jan Engelhardt <jengelh [at] linux01 gwdg de>, 2002 - 2005
#   -- License restrictions apply (GPL v2)
#
#   This file is part of tmctl.
#   tmctl is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by
#   the Free Software Foundation; however ONLY version 2 of the License.
#
#   tmctl is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program kit; if not, write to:
#   Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
#   Boston, MA  02110-1301  USA
#
#   -- For details, see the file named "LICENSE.GPL2"
#==============================================================================

include makevars.inc

CFLAGS  += -I../libxlg/src
LDFLAGS += -L../libxlg/src -Wl,-rpath,../libxlg/src

all: tm2ppm tm2txt

tm2ppm: tm2ppm.o tables.o
	${VECHO_LD}
	${LD} ${LDFLAGS} -o $@ $^ -lHX -lxlg;
	${STRIP} -s $@;

tm2txt: tm2txt.o tables.o
	${VECHO_LD}
	${LD} ${LDFLAGS} -o $@ $^ -lHX;
	${STRIP} -s $@;

%.o: %.c
	${VECHO_CC}
	${CC} ${CFLAGS} -Wp,-MMD,.$*.d -o $@ -c $<;

%.o: %.cpp
	${VECHO_CXX}
	${CXX} ${CXXFLAGS} -Wp,-MMD,.$*.d -o $@ -c $<;

clean:
	rm -f *.o tm2ppm tm2txt *.bbg *.da *.gcov gmon.out;

distclean: clean
	rm -f .*.d;

-include .*.d

#==============================================================================
