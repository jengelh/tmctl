/*=============================================================================
tmctl - Conrad Telemetrics Unit data capturing and analyzation tools
  Copyright © Jan Engelhardt <jengelh [at] linux01 gwdg de>, 2002 - 2005
  -- License restrictions apply (GPL v2)

  This file is part of tmctl.
  tmctl is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; however ONLY version 2 of the License.

  tmctl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program kit; if not, write to:
  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
  Boston, MA  02110-1301  USA

  -- For details, see the file named "LICENSE.GPL2"
=============================================================================*/
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <libHX.h>
#include "disk.h"
#include "tables.h"
#define MAXFNLEN 256

//-----------------------------------------------------------------------------
int main(int argc, char **argv) {
    while(*++argv != NULL) {
        struct logentry le;
        int fd;

        if((fd = open(*argv, O_RDONLY)) < 0) {
            fprintf(stderr, "Could not open %s: %s\n", *argv, strerror(errno));
            continue;
        }

        while(read(fd, &le, sizeof(struct logentry)) > 0) {
            struct tm res;
            char buf[512];
            time_t t = le.st - 7 * 3600;

            localtime_r(&t, &res);
            strftime(buf, 512, "%d.%m.%Y %H:%M:%S", &res);
            printf("Z:%s, T:%5.1f C, P:%4hu, H:%3hu%%, L:%6hu\n",
                buf,
                trans_temp[le.data.analog[CH_TEMP]],
                trans_baro[le.data.analog[CH_BARO]],
                trans_hygro[le.data.analog[CH_HYGRO]],
                trans_lux[le.data.analog[CH_LUX]]
            );
        }
        close(fd);
    }
    return EXIT_SUCCESS;
}

//=============================================================================
