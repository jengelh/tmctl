/*=============================================================================
tmctl - Conrad Telemetrics Unit data capturing and analyzation tools
  Copyright © Jan Engelhardt <jengelh [at] linux01 gwdg de>, 2002 - 2005
  -- License restrictions apply (GPL v2)

  This file is part of tmctl.
  tmctl is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; however ONLY version 2 of the License.

  tmctl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program kit; if not, write to:
  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
  Boston, MA  02110-1301  USA

  -- For details, see the file named "LICENSE.GPL2"
=============================================================================*/
/*
    This is the only program intended to compile with Borland Turbo C++.
    Data processing should be done on a GNU System (Linux, MinGW or Cygwin)
*/
#include <sys/timeb.h>
#include <conio.h>
#include <ctype.h>
#include <dos.h>
#include <errno.h>
#include <graphics.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "disk.h"
#include "tables.h"
#define S_FONT  TRIPLEX_FONT
#define S_FONTR triplex_font_far
#define TR_X(val) ((double)gxdev.wd * (val))
#define TR_Y(val) ((double)gxdev.hg * (val))

typedef unsigned char byte;
typedef unsigned short Ushort;

static struct {
    int driver, mode, err, wd, hg, cl;
    char inited;
} gxdev;

static struct logentry frame;

static byte
    *REG_baro  = &frame.data.analog[CH_BARO],
    *REG_hygro = &frame.data.analog[CH_HYGRO],
    *REG_lux   = &frame.data.analog[CH_LUX],
    *REG_temp  = &frame.data.analog[CH_TEMP];

static int init_gxdev(byte *);
static void deinit_gxdev(byte *);
static void init_serial_port(Ushort);
static int gprintf(Ushort, Ushort, byte *, ...);
static FILE *flush_log(FILE **, byte *);

//-----------------------------------------------------------------------------
int main(int argc, char **argv) {
    uint16_t port = 0x3F8;
    char *logfile = "tmet.out";
    int verbose = 1;
    FILE *logp = NULL;

    { // parse arguments
        char **travp = argv, *next;
        while(--argc) {
            if(strcmp(*travp, "-f") == 0) {
                next = *(travp + 1);
                if(next != NULL) {
                    logfile = next;
                    ++travp;
                }
            } else if(strcmp(*travp, "-p") == 0) {
                next = *(travp + 1);
                if(memicmp(next, "COM", 3) == 0 && next[3] != '\0') {
                    if((port = *(Ushort far *)(0x400 + next[4] - '1')) == 0) {
                        fprintf(stderr, "Port %s not installed. "
                         "Try \"0xADDR\" for override.\n", next);
                        return 1;
                    }
                } else {
                    port = strtoul(next, NULL, 0);
                }
                ++travp;
            } else if(strcmp(*travp, "-q") == 0) {
                verbose = 0;
            } else if(strcmp(*travp, "-v") == 0) {
                verbose = 1;
            }
            ++travp;
        }
    }

    flush_log(&logp, logfile);
  
    init_serial_port(port);
    printf(
        "TELEMET DATA CAPTURING TOOL\n"
        "version 1.03 (may 29 2005) :: jengelh@linux01.gwdg.de\n"
        "Acquiring data. Hit space bar for info.\n"
    );

    // ----------------------------------------
    {
        byte capturing = 0, captured_column = 0, run = 1;
        unsigned long data_frame_count = 0;
        struct timeb timer_start, timer_end;
        ftime(&timer_start);

        while(run) { // main loop
            if(inportb(port + 5) & 1) { // received a byte
                double diff;
                ftime(&timer_end);
                diff = (timer_end.time + (double)timer_end.millitm / 1000) -
                 (timer_start.time + (double)timer_start.millitm / 1000);
                ftime(&timer_start);

                // time interval between data frames is at least 300 msec
                if(diff > 0.3) {
                    captured_column = 0;
                    capturing = 1;
                }
                ((byte *)&frame.data)[captured_column++] = inportb(port);
            }

            if(captured_column == 14 && capturing) { // finished frame
                captured_column = 0;
                frame.st = timer_start.time;
                frame.stm = timer_start.millitm;
                fwrite(&frame, sizeof(frame), 1, logp);
                if(!gxdev.inited && verbose) {
                    signed char j;
                    if(data_frame_count++ % 22 == 0) {
                        printf(
                          "|  SYS TIME | TMETTIME | ANALOG                          | DIGITAL  |\n"
                          "|           |          |  #1  #2  #3  #4  #5  #6  #7  #8 | 76543210 |\n"
                          ">-----------+----------+---------------------------------+----------<\n"
                        );
                    }
                    printf("| %9ld | %02hu:%02hu.%02hu |", frame.st,
                     frame.data.min, frame.data.sec, frame.data.msec);
                    for(j = 0; j < 8; ++j) {
                        printf(" %3hu", frame.data.analog[j]);
                    }
                    printf(" | ");
                    for(j = 7; j >= 0; --j) {
                        printf("%c", (frame.data.digital & (1 << j)) ? '*' : '.');
                    }
                    printf(" |\n");
                } else if(gxdev.inited) {
                    static int old_baro = 0, old_hygro = 0, old_lux = 0, old_temp = 0;
                    if(old_temp < 0) {
                        clearviewport();
                    } else {
                        setcolor(0);
                        if(old_temp != *REG_temp) { gprintf(TR_X(0.5), TR_Y(0.1), "%.1f C", trans_temp[old_temp]); }
                        if(old_baro != *REG_baro) { gprintf(TR_X(0.5), TR_Y(0.3), "%hu hPa", trans_baro[old_baro]); }
                        if(old_hygro != *REG_hygro) { gprintf(TR_X(0.5), TR_Y(0.5), "%hu %", trans_hygro[old_hygro]); }
                        if(old_lux != *REG_lux) { gprintf(TR_X(0.5), TR_Y(0.7), "%lu Lux", trans_lux[old_lux]); }
                    }

                    setcolor(15);
                    gprintf(TR_X(0.5), TR_Y(0.1), "%.1f C", trans_temp[old_temp = *REG_temp]);
                    gprintf(TR_X(0.5), TR_Y(0.3), "%hu hPa", trans_baro[old_baro = *REG_baro]);
                    gprintf(TR_X(0.5), TR_Y(0.5), "%hu %", trans_hygro[old_hygro = *REG_hygro]);
                    gprintf(TR_X(0.5), TR_Y(0.7), "%lu Lux", trans_lux[old_lux = *REG_lux]);
                }
                if(++data_frame_count % 1000 == 0) {
                    flush_log(&logp, logfile);
                }
            } // completed frame

            if(kbhit()) { // handle keyboard events
                int k = tolower(getch());
                switch(k) {
                    case 'f':
                        if(init_gxdev(&gxdev.inited)) {
                            settextstyle(S_FONT, HORIZ_DIR, 8);
                            settextjustify(CENTER_TEXT, CENTER_TEXT);
                            setfillstyle(SOLID_FILL, 15);
                            setcolor(15);
                            gprintf(TR_X(0.5), TR_Y(0.5), "* WAIT *");
                        } else {
                            deinit_gxdev(&gxdev.inited);
                        }
                        break;
                    case 'l':
                        flush_log(&logp, logfile);
                        break;
                    case 'q':
                        run = 0;
                        deinit_gxdev(&gxdev.inited);
                        break;
                    case 'v':
                        verbose = !verbose;
                        if(!gxdev.inited) {
                            fprintf(stderr, "Toggled verbosity to %s\n",
                             verbose ? "on" :  "off");
                        }
                        break;
                    default:
                        printf(
                          "Using COM port 0x%04X (received %lu data frames)\n"
                          "Commands: [F]ullscreen; Flush [l]og; "
                          "Toggle [v]erbosity; [Q]uit\n",
                          port, data_frame_count
                        );
                        break;
                } // switch()
            } // handle keyboard events
        } // main loop
    }

    fclose(logp);
    return 0;
}

//-----------------------------------------------------------------------------
static int init_gxdev(byte *inited) {
    gxdev.driver = DETECT;
    registerfarbgidriver(EGAVGA_driver_far);
    registerfarbgifont(S_FONTR);
    initgraph(&gxdev.driver, &gxdev.mode, NULL);
    if((gxdev.err = graphresult()) != grOk) {
        fprintf(stderr, "Graphics error: %s\n", grapherrormsg(gxdev.err));
        return 0;
    }

    gxdev.cl = getmaxcolor() + 1;
    gxdev.wd = getmaxx();
    gxdev.hg = getmaxy();
    if(inited != NULL) {
        *inited = 1;
    }
    return 1;
}

static void deinit_gxdev(byte *inited) {
    closegraph();
    if(inited != NULL) {
        *inited = 0;
    }
    return;
}

static void init_serial_port(Ushort port) {
    // Initialize COM port, with 2400 baud, no parity, 8 data bits, 1 stop bit
    // see http://beyondlogic.org/ for details
    outportb(port + 1, 0);    // turn of interrupts
    outportb(port + 3, 0x80); // set DLAB on
    outportb(port, 0x30);     // baud rate (lo)
    outportb(port + 1, 0x00); // baud rate (hi)
    outportb(port + 3, 0x03); // 8 data bits, 1 stop bit
    outportb(port + 2, 0xC7);
    outportb(port + 4, 0x0B);
    return;
}

static int gprintf(Ushort lx, Ushort ly, byte *fmt, ...) {
    byte buf[1024];
    int ret;
    va_list ap;
    va_start(ap, fmt);
    ret = vsprintf(buf, fmt, ap); // risk
    outtextxy(lx, ly, buf);
    va_end(ap);
    return ret;
}

static FILE *flush_log(FILE **fp, byte *fn) {
    if(*fp != NULL) {
        fclose(*fp);
    }
    if((*fp = fopen(fn, "ab")) == NULL) {
        fprintf(stderr, "Could not open %s: %s\a\n", fn, strerror(errno));
    }
    return *fp;
}

//=============================================================================
