/*=============================================================================
tmctl - Conrad Telemetrics Unit data capturing and analyzation tools
  Copyright © Jan Engelhardt <jengelh [at] linux01 gwdg de>, 2002 - 2006
  -- License restrictions apply (GPL v2)

  This file is part of tmctl.
  tmctl is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; however ONLY version 2 of the License.

  tmctl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program kit; if not, write to:
  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
  Boston, MA  02110-1301  USA

  -- For details, see the file named "LICENSE.GPL2"
=============================================================================*/
/*
    This is the only program intended to compile with Borland Turbo C++.
    Data processing should be done on a GNU System (Linux, MinGW or Cygwin)
*/
#include <sys/timeb.h>
#include <conio.h>
#include <ctype.h>
#include <dos.h>
#include <errno.h>
#include <graphics.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "disk.h"
#include "tables.h"
#define S_FONT          TRIPLEX_FONT
#define S_FONTR         triplex_font_far
#define TR_X(val)       ((double)gxdev.wd * (val))
#define TR_Y(val)       ((double)gxdev.hg * (val))

enum {
    /* The start state is S_WAIT. In case the Telemetric Unit is currently
    sending while the program starts, we must wait for the start boundary of
    the next frame. */
    S_WAIT = 0,
    S_RUNNING,
};

static struct {
    int driver, mode, err, wd, hg, cl;
    int inited;
} gxdev;

struct options {
    char *logfile, *serport;
    int verbose;
};

// Functions
static void mainloop(FILE *, uint16_t);
static void dump_frame(const struct logentry *, FILE **, unsigned long *);
static FILE *flush_log(FILE *);
static void init_serial_port(uint16_t);

static void check_serport(const struct HXoptcb *);
static int get_options(int *, const char ***, struct options *);

// Variables. Yeah, globals are bad.
static const char *Logfile = "tmet.out";
static int         Verbose = 1;
static uint16_t    SerPort = 0x3F8;
static struct logentry frame;

static uint8_t
    *Reg_baro  = &frame.data.analog[CH_BARO],
    *Reg_hygro = &frame.data.analog[CH_HYGRO],
    *Reg_lux   = &frame.data.analog[CH_LUX],
    *Reg_temp  = &frame.data.analog[CH_TEMP];

//-----------------------------------------------------------------------------
int main(int argc, const char **argv) {
    struct options opt;
    FILE *fp;
    int serfd;

    if(!get_options(&argc, &argv, &opt))
        return EXIT_FAILURE;
    if((serfd = init_serial_port(opt.serport)) < 0)
        return EXIT_FAILURE;

    printf(
        "TELEMET DATA CAPTURING TOOL\n"
        "version 3 (june 05 2006) :: jengelh@linux01.gwdg.de\n"
        "Acquiring data. Hit space bar for info.\n"
    );

    fp = flush_log(NULL);
    mainloop(fp, SerPort);
    if(fp != NULL)
        fclose(fp);
    return 0;
}

static void mainloop(FILE *logp, uint16_t port) {
    int column = 0, ml_running = 1;
    struct timeb timer_start, timer_end;
    struct logentry frame;
    unsigned long frame_count = 0;
    unsigned int state = S_WAIT;

    ftime(&timer_start);

    while(ml_running) {
        // Received a byte
        if(inportb(port + 5) & 1) {
            short di;

            ftime(&timer_end);
            di = timer_end.millitm - timer_start.millitm;
            if(di < 0)
                di += 1000;
            if(di > 300) {
                if(state == S_RUNNING && column < 14) {
                    /* For some reason, something took longer than 300 msecs
                    and the frame could not be finished. Invalidate the
                    remaining columns and dump the frame. Keep the current
                    byte in the UART buffer by skipping inportb(port). */
                    memset(&frame.data.analog[column], 0, 14 - column);
                    dump_frame(&frame, &logp, &frame_count);
                }
                column = 0;
                state  = S_RUNNING;
            }
            ((uint8_t *)&frame.data)[column++] = inportb(port);
        }

        // Frame successfully completed
        if(column == 14 && state == S_RUNNING) {
            frame.st  = timer_start.time;
            frame.stm = timer_start.millitm;
            column    = 0;
            dump_frame(&frame, &logp, &frame_count);
        }

        // Now handle keyboard events
        if(kbhit()) {
            int k = tolower(getch());
            switch(k) {
                case 'f':
                    gxdev.inited = switch_gxdev(!gxdev.inited);
                    if(gxdev.inited) {
                        settextstyle(S_FONT, HORIZ_DIR, 8);
                        settextjustify(CENTER_TEXT, CENTER_TEXT);
                        setfillstyle(SOLID_FILL, 15);
                        setcolor(15);
                        gprintf(TR_X(0.5), TR_Y(0.5), "* WAIT *");
                    }
                    break;
                case 'l':
                    logp = flush_log(logp);
                    break;
                case 'q':
                    ml_running = 0;
                    gxdev.inited = switch_gxdev(0);
                    break;
                case 'v':
                    Verbose = !Verbose;
                    if(!gxdev.inited)
                        fprintf(stderr, "Toggled verbosity to %s\n",
                         Verbose ? "on" :  "off");
                    break;
                default:
                    printf(
                      "Using COM port 0x%04X (received %lu data frames)\n"
                      "Commands: [F]ullscreen; Flush [l]og; "
                      "Toggle [v]erbosity; [Q]uit\n",
                      port, frame_count
                    );
                    break;
            }
        }
    }
    return;
}

//-----------------------------------------------------------------------------
static void dump_frame(const struct logentry *frame, FILE **logp,
 unsigned long *count)
{
    int j;

    fwrite(frame, sizeof(struct logentry), 1, logp);
    if(++*count % 1024 == 0)
        *logp = flush_log(*logp);

    if(!gxdev.inited && Verbose) {
        if((*count)++ % 22 == 0) {
            printf(
              "|  SYS TIME | TMETTIME | ANALOG                          | DIGITAL  |\n"
              "|           |          |  #1  #2  #3  #4  #5  #6  #7  #8 | 76543210 |\n"
              ">-----------+----------+---------------------------------+----------<\n"
            );
        }
        printf("| %9ld | %02hu:%02hu.%02hu |", frame->st,
         frame->data.min, frame->data.sec, frame->data.msec);
        for(j = 0; j < 8; ++j)
            printf(" %3hu", frame->data.analog[j]);
        printf(" | ");
        for(j = 7; j >= 0; --j)
            printf("%c", (frame->data.digital & (1 << j)) ? '*' : '.');
        printf(" |\n");
    } else if(gxdev.inited) {
        static int old_baro = 0, old_hygro = 0, old_lux = 0, old_temp = 0;
        if(old_temp < 0) {
            clearviewport();
        } else {
            setcolor(0);
            if(old_temp != *Reg_temp)
                gprintf(TR_X(0.5), TR_Y(0.1), "%.1f C", trans_temp[old_temp]);
            if(old_baro != *Reg_baro)
                gprintf(TR_X(0.5), TR_Y(0.3), "%hu hPa", trans_baro[old_baro]);
            if(old_hygro != *Reg_hygro)
                gprintf(TR_X(0.5), TR_Y(0.5), "%hu %", trans_hygro[old_hygro]);
            if(old_lux != *Reg_lux)
                gprintf(TR_X(0.5), TR_Y(0.7), "%lu Lux", trans_lux[old_lux]);
        }

        setcolor(15);
        gprintf(TR_X(0.5), TR_Y(0.1), "%.1f C", trans_temp[old_temp = *Reg_temp]);
        gprintf(TR_X(0.5), TR_Y(0.3), "%hu hPa", trans_baro[old_baro = *Reg_baro]);
        gprintf(TR_X(0.5), TR_Y(0.5), "%hu %", trans_hygro[old_hygro = *Reg_hygro]);
        gprintf(TR_X(0.5), TR_Y(0.7), "%lu Lux", trans_lux[old_lux = *Reg_lux]);
    }
    return;
}

static FILE *flush_log(FILE *fp) {
    if(fp != NULL)
        fclose(fp);
    if((fp = fopen(Logfile, "ab")) == NULL)
        fprintf(stderr, "Could not open %s: %s\a\n", Logfile, strerror(errno));
    return fp;
}

static void init_serial_port(const char *port) {
    struct termios tty;
    int fd;

    if((fd = open(port, O_RDWR)) < 0) {
        fprintf(stderr, "Could not open %s: %s\n", port, strerror(errno));
        return -1;
    }

    tcgetattr(fd, &tty);
    /* Initialize COM port, with 2400 baud, no parity, 8 data bits, 1 stop bit.
    See http://beyondlogic.org/ for details. A good guide. */
    cfsetispeed(&tty, B2400);
    cfsetospeed(&tty, B2400);
//    tty.c_iflag = IGNBRK | IGNPAR;
    tty.c_cflag = (tty.c_cflags & ~CSIZE) | CS8;
    tcsetattr(fd, TCSANOW, &tty);
    return;
}

static void check_serport(const struct HXoptcb *cbi) {
    long port = strtoul(cbi->s, NULL, 0);
    int fd;

    if(port == 0)
        return;
    printf("Specified serial port at ioaddr 0x%x\n", port);
    return;
}
static int get_options(int *argc, const char ***argv, struct options *op) {
    struct HXoption options_table[] = {
        {.sh = 'f', .type = HXTYPE_STRING, .ptr = &op->logfile,
         .help = "Output logfile", .htyp = "FILE"},
        {.sh = 'p', .type = HXTYPE_STRING, .ptr = &op->serport,
         .cb = check_serport,
         .help = "Serial port device (e.g. /dev/ttyS0)", .htyp = "FILE"},
        {.sh = 'q', .type = HXTYPE_VAL, .val = 0, .ptr = &op->verbose,
         .help = "Be quiet"},
        {.sh = 'v', .type = HXTYPE_VAL, .val = 1, .ptr = &op->verbose,
         .help = "Be verbose"},
        HXOPT_AUTOHELP,
        HXOPT_TABLEEND,
    }
    return HX_getopt(options_table, argc, argv, HXOPT_USAGEONERR) > 0;
}

//=============================================================================
