Warning:
  @echo WARNING - prj2mak
  @echo Not found in the library path : egavgaf.obj
  @echo Not found in the library path : tripf.obj
  @echo Please correct the path of these elements in the makefile,
  @echo and delete this warning section

.AUTODEPEND

#		*Translator Definitions*
CC = tcc +TMCTL.CFG
TASM = TASM
TLINK = tlink


#		*Implicit Rules*
.c.obj:
  $(CC) -c {$< }

.cpp.obj:
  $(CC) -c {$< }

.asm.obj:
  $(TASM) {$< }


#		*List Macros*


EXE_dependencies =  \
  tmctl.obj \
  tables.obj \
  c:\tcpp\lib\graphics.lib \
  egavgaf.obj \
  tripf.obj

#		*Explicit Rules*
tmctl.exe: tmctl.cfg $(EXE_dependencies)
  $(TLINK) /v/x/c/d @&&|
c:\tcpp\lib\c0l.obj+
tmctl.obj+
tables.obj+
egavgaf.obj+
tripf.obj
tmctl,
c:\tcpp\lib\graphics.lib+
c:\tcpp\lib\fp87.lib+
c:\tcpp\lib\mathl.lib+
c:\tcpp\lib\cl.lib
|


#		*Individual File Dependencies*
tmctl.obj: tmctl.c 

tables.obj: tables.c 

#		*Compiler Configuration File*
tmctl.cfg: tmctl.mak
  copy &&|
-ml
-2
-f287
-v
-G
-O
-V
-vi-
-wamb
-wamp
-wpro
-wdef
-w-rng
-wnod
-wstv
-wuse
-IC:\TCPP\INCLUDE
-LC:\TCPP\LIB
| tmctl.cfg


