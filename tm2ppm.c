/*=============================================================================
tmctl - Conrad Telemetrics Unit data capturing and analyzation tools
  Copyright © Jan Engelhardt <jengelh [at] linux01 gwdg de>, 2002 - 2005
  -- License restrictions apply (GPL v2)

  This file is part of tmctl.
  tmctl is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; however ONLY version 2 of the License.

  tmctl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program kit; if not, write to:
  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
  Boston, MA  02110-1301  USA

  -- For details, see the file named "LICENSE.GPL2"
=============================================================================*/
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <xlg.h>
#include "disk.h"
#include "tables.h"
#define MAXFNLEN 256

typedef void (*color_func_t)(uint8_t, uint8_t *, uint8_t *, uint8_t *);
enum {
    G_NONE     = 0,
    G_STRETCH  = 1 << 0,
    G_RDX9     = 1 << 1,
    G_TOPOBAND = 1 << 2,
    G_TOPOIZE  = 1 << 3,

    MK_NONE   = 0,
    MK_MONTH  = 1 << 0,
    MK_HOURS  = 1 << 1,
};

static void gen(struct logentry *, long, int, unsigned int, color_func_t, const char *);
static void CC_hygro(uint8_t, uint8_t *, uint8_t *, uint8_t *);
static void CC_red(uint8_t, uint8_t *, uint8_t *, uint8_t *);
static void CC_thermal(uint8_t, uint8_t *, uint8_t *, uint8_t *);
static void CC_thermal2(uint8_t, uint8_t *, uint8_t *, uint8_t *);
static void img_add_markers(XLG_pic *, unsigned int);
static unsigned int img_average(XLG_pic *, unsigned int);
static void img_colorize(XLG_pic *, color_func_t);
static void img_hgredux(XLG_pic *, double);
static void img_stretch(XLG_pic *);
static void img_topologize(XLG_pic *, int, unsigned int);
static size_t read_datasets(const char **, void **);

//-----------------------------------------------------------------------------
int main(int argc, const char **argv) {
    struct logentry *d;
    size_t n;

    ++argv;
    n = read_datasets(argv, (void **)&d);

    gen(d, n, CH_0,     G_NONE,    NULL,       "OUT_ch0_GF.ppm");
    gen(d, n, CH_BARO,  G_NONE,    NULL,       "OUT_baro_GF.ppm");
    gen(d, n, CH_BARO,  G_STRETCH, CC_hygro,   "OUT_baro_KS.ppm");
    gen(d, n, CH_HYGRO, G_NONE,    NULL,       "OUT_hygro_GF.ppm");
    gen(d, n, CH_HYGRO, G_STRETCH, CC_hygro,   "OUT_hygro_KS.ppm");
    gen(d, n, CH_3,     G_NONE,    NULL,       "OUT_ch3_GF.ppm");
    gen(d, n, CH_3,     G_STRETCH, CC_red,     "OUT_ch3_KS.ppm");
    gen(d, n, CH_4,     G_NONE,    NULL,       "OUT_ch4_GF.ppm");
    gen(d, n, CH_4,     G_STRETCH, CC_red,     "OUT_ch4_KS.ppm");
    gen(d, n, CH_LUX,   G_NONE,    NULL,       "OUT_lux_GF.ppm");
    gen(d, n, CH_LUX,   G_STRETCH, NULL,       "OUT_lux_GS.ppm");
    gen(d, n, CH_LUX,   G_STRETCH, CC_hygro,   "OUT_lux_KS.ppm");
    gen(d, n, CH_6,     G_NONE,    NULL,       "OUT_ch6_GF.ppm");
    gen(d, n, CH_6,     G_STRETCH, CC_red,     "OUT_ch6_KS.ppm");
    gen(d, n, CH_TEMP,  G_NONE,    NULL,       "OUT_temp1_GF.ppm");
    gen(d, n, CH_TEMP,  G_STRETCH, NULL,       "OUT_temp1_GS.ppm");
    gen(d, n, CH_TEMP,  G_NONE,    CC_thermal, "OUT_temp1_KF.ppm");
    gen(d, n, CH_TEMP,  G_STRETCH | G_RDX9,
                                   CC_thermal, "OUT_temp1_KS.ppm");
    gen(d, n, CH_TEMP,  G_STRETCH | G_RDX9 | G_TOPOBAND,
                                   CC_thermal2, "OUT_temp2_KS.ppm");
    gen(d, n, CH_TEMP,  G_STRETCH | G_RDX9 | G_TOPOBAND | G_TOPOIZE,
                                   CC_thermal2, "OUT_temp3_KS.ppm");
    gen(d, n, CH_TEMP,  G_STRETCH | G_RDX9 | G_TOPOBAND | G_TOPOIZE,
                                   CC_thermal, "OUT_temp4_KS.ppm");
    return EXIT_SUCCESS;
}

static void gen(struct logentry *frame, long dcount, int tm_channel,
 unsigned int flags, color_func_t cf, const char *file)
{
    unsigned int topo_stripe_size = !!(flags & G_TOPOBAND) * 8;
    XLG_pic *pic = xlg_pic_new(365, 144 + topo_stripe_size);

    printf("Generating %-20s from channel %d\n",
     file, tm_channel);

    while(dcount--) {
        time_t le_time = frame->st;
        struct tm sv;
	long x, y;

        le_time -= 7 * 3600; // our tmet unit is fux0red?

	localtime_r(&le_time, &sv);
	x = sv.tm_yday;
        y = (sv.tm_hour * 3600 + sv.tm_min * 60 + sv.tm_sec) / 600;

	fprintf(stderr, "%04d-%02d-%02d %02d:%02d:%02d %u\n",
		sv.tm_year+1900, sv.tm_mon+1, sv.tm_mday+1,
		sv.tm_hour, sv.tm_min, sv.tm_sec, frame->data.analog[tm_channel]);
        xlg_pic_setpx_sw(pic, x, y + topo_stripe_size,
         frame->data.analog[tm_channel]);
	++frame;
    }

    // Postprocess
    printf("  Average value: %u\n", img_average(pic, topo_stripe_size));

    if(flags & G_STRETCH)
        img_stretch(pic);

    if(flags & G_TOPOBAND) {
        int xpos;
        for(xpos = 0; xpos < 365; ++xpos) {
            unsigned int col = xlg_pv_squash(xpos * 253 / 365) + 2;
            int ypos;
            for(ypos = 0; ypos < topo_stripe_size - 1; ++ypos)
                xlg_pic_setpx_sw(pic, xpos, ypos, col);
        }
    }
    if(flags & G_TOPOIZE)
        img_topologize(pic, 8, topo_stripe_size);
    if(flags & G_RDX9)
        img_hgredux(pic, 0.9);
    if(cf != NULL)
        img_colorize(pic, cf);

    //img_add_markers(pic, MK_MONTH | MK_HOURS);
    xlg_pic_store(pic, file);
    xlg_pic_free(pic);
    return;
}

//-----------------------------------------------------------------------------
static void CC_hygro(uint8_t i, uint8_t *rp, uint8_t *gp, uint8_t *bp) {
    // (0,0,0) - (0,128,255) - (255,255,255)

    if(i == 0) {
        *rp = *gp = *bp = 0;
        return;
    }

    if(i <= 128) {
	*rp = 0;
        *gp = xlg_pv_squash(i);
	*bp = xlg_pv_squash(2 * i);
        return;
    }

    i  -= 128;
    *rp = xlg_pv_squash(2 * i);
    *gp = xlg_pv_squash(128 + i);
    *bp = 255;
    return;
}

static void CC_red(uint8_t i, uint8_t *rp, uint8_t *gp, uint8_t *bp) {
    if(i == 0) {
        *rp = *gp = *bp = 0;
        return;
    }

    *rp = i;
    *gp = *bp = 0;
    return;
}

static void CC_thermal(uint8_t i, uint8_t *rp, uint8_t *gp, uint8_t *bp) {
    // (0,64,0) - (255,255,128) - (255,0,0)

    if(i == 0) {
        *rp = *gp = *bp = 0;
        return;
    }

    if(i <= 128) {
	*rp = xlg_pv_squash(2 * i);
        *gp = xlg_pv_squash(128 + i);
	*bp = xlg_pv_squash(i);
        return;
    }

    i  -= 128;
    *rp = 255;
    *gp = xlg_pv_squash(255 - 2 * i);
    *bp = xlg_pv_squash(128 - i);
    return;
}

static void CC_thermal2(uint8_t i, uint8_t *rp, uint8_t *gp, uint8_t *bp) {
    // (0,128,0) - (255,255,0) - (255,0,0)

    if(i == 0) {
        *rp = *gp = *bp = 0;
        return;
    }

    if(i <= 128) {
	*rp = xlg_pv_squash(2 * i);
        *gp = xlg_pv_squash(64 + 2 * i);
	*bp = 0;
        return;
    }

    i  -= 128;
    *rp = 255;
    *gp = xlg_pv_squash(255 - 2 * i);
    *bp = 0;
    return;
}

static void img_add_markers(XLG_pic *pic, unsigned int flags) {
#define sp(p, x, y) xlg_pic_setpx((p), (x), (y), 0, 0, 255);
    if(flags & MK_MONTH) {
        unsigned int dc = 0;
        size_t y = pic->sy / 2;
        sp(pic, dc,       y);
        sp(pic, dc += 31, y);
        sp(pic, dc += 29, y);
        sp(pic, dc += 31, y);
        sp(pic, dc += 30, y);
        sp(pic, dc += 31, y);
        sp(pic, dc += 30, y);
        sp(pic, dc += 31, y);
        sp(pic, dc += 31, y);
        sp(pic, dc += 30, y);
        sp(pic, dc += 31, y);
        sp(pic, dc += 30, y);
    }
    if(flags & MK_HOURS) {
        size_t x = pic->sx / 2, y;
        for(y = 0; y < pic->sy; y += pic->sy * 4 / 24)
            sp(pic, x, y);
    }
    return;
#undef sp
}

static unsigned int img_average(XLG_pic *pic, unsigned int sh) {
    int x, y;
    unsigned long total = 0;
    for(y = sh; y < pic->sy; ++y)
        for(x = 0; x < pic->sx; ++x)
            total += xlg_pic_getpx_sw(pic, x, y);
    total /= (pic->sy - sh) * pic->sx;
    return total;
}

static void img_colorize(XLG_pic *pic, color_func_t cf) {
    int x, y;
    for(y = 0; y < pic->sy; ++y) {
        for(x = 0; x < pic->sx; ++x) {
            uint8_t lum = xlg_pic_getpx_sw(pic, x, y), r, g, b;
            cf(lum, &r, &g, &b);
            xlg_pic_setpx(pic, x, y, r, g, b);
        }
    }
    return;
}

static void img_hgredux(XLG_pic *pic, double pct) {
    /*
        Reduce the histogram height by multiplying all pixels
        with a value < 1.0
    */
    int x, y;

    for(y = 0; y < pic->sy; ++y) {
        for(x = 0; x < pic->sx; ++x) {
            uint8_t lum = xlg_pic_getpx_sw(pic, x, y);
            if(lum == 0)
                continue;
            xlg_pic_setpx_sw(pic, x, y, xlg_pv_squash(pct * lum));
        }
    }
    return;
}

static void img_stretch(XLG_pic *pic) {
    /*
        Stretch the histogram
        - not keeping baseline pixel
        - special handling for gray(0)
    */
    double fac;
    int x, y, min = -1, max = -1;

    // Pass I: Find lightest / darkest values
    for(y = 0; y < pic->sy; ++y) {
        for(x = 0; x < pic->sx; ++x) {
            uint8_t lum = xlg_pic_getpx_sw(pic, x, y);
            if(lum == 0)
                continue; // "no data"
            if(lum < min || min == -1)
                min = lum;
            if(lum > max || max == -1)
                max = lum;
        }
    }

    if(min == -1 && max == -1)
        return;
    printf("  min=%d max=%d\n", min, max);
    fac = 255.0 / (max - min);

    for(x = 0; x < pic->sx; ++x) {
        for(y = 0; y < pic->sy; ++y) {
            short a = xlg_pic_getpx_sw(pic, x, y);
            short r = xlg_pv_squash(fac * xlg_pv_squash(a - min));
            xlg_pic_setpx_sw(pic, x, y, r);
        }
    }

    return;
}

static void img_topologize(XLG_pic *pic, int num, unsigned int sh) {
    uint8_t lev[256];
    int i, x, y;

    lev[0] = 0;
    for(i = 0; i < 256; ++i) {
        int sec = i * num / 256;
        sec *= 256 / num;
        sec += 128 / num;
        lev[i] = sec;
    }
    for(y = 0; y < pic->sy; ++y)
        for(x = 0; x < pic->sx; ++x)
            xlg_pic_setpx_sw(pic, x, y, lev[xlg_pic_getpx_sw(pic, x, y)]);
    return;
}

static size_t read_datasets(const char **argv, void **datap) {
    size_t nument = 0, db_size = 0;
    void *data = NULL;

    while(*argv != NULL) {
        volatile size_t tf_count, tf_size;
        struct stat sb;
        int ret, fd;

        if((fd = open(*argv, O_RDONLY)) < 0) {
            fprintf(stderr, "Could not open %s: %s\n",
             *argv++, strerror(errno));
            continue;
        }

        if(fstat(fd, &sb) < 0) {
            fprintf(stderr, "Could not stat fd of %s: %s\n",
             *argv++, strerror(errno));
            close(fd);
            continue;
        }

        // Allocate more space
        tf_count = sb.st_size / sizeof(struct logentry);
        tf_size  = tf_count * sizeof(struct logentry);
        if((data = realloc(data, db_size + tf_size)) == NULL) {
            perror("realloc()");
            exit(EXIT_FAILURE);
        }
        memset(data + db_size, tf_size, '\0');

        // Read from file
        ret = read(fd, data + db_size, tf_size);
        if(ret < 0) {
            fprintf(stderr, "Could not read on %s: %s\n",
             *argv++, strerror(errno));
            close(fd);
            continue;
        }
        if(ret < tf_size)
            fprintf(stderr, "Warning: Short read from %s\n", *argv);

        close(fd);
        tf_count = ret / sizeof(struct logentry);
        tf_size  = tf_count * sizeof(struct logentry);
        db_size += tf_size;
        nument  += tf_count;
        fprintf(stderr, "%s: %ld bytes (%ld frames)\n",
         *argv, (long)tf_size, (long)tf_count);
        ++argv;
    }

    fprintf(stderr, "TOTAL: %ld bytes (%ld frames)\n",
     (long)db_size, (long)nument);
    *datap = data;
    return nument;
}

//=============================================================================
