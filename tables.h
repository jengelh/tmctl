/*=============================================================================
tmctl - Conrad Telemetrics Unit data capturing and analyzation tools
  Copyright © Jan Engelhardt <jengelh [at] linux01 gwdg de>, 2002 - 2005
  -- License restrictions apply (GPL v2)

  This file is part of tmctl.
  tmctl is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; however ONLY version 2 of the License.

  tmctl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program kit; if not, write to:
  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
  Boston, MA  02110-1301  USA

  -- For details, see the file named "LICENSE.GPL2"
=============================================================================*/
#ifndef TMET_TABLES_H
#define TMET_TABLES_H 1

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

extern uint16_t trans_baro[];
extern uint16_t trans_h510[];
extern uint16_t trans_high2401[];
extern uint8_t  trans_hygro[];
extern uint32_t trans_lux[];
extern uint8_t  trans_spl[];
extern uint8_t  trans_speed230[];
extern int16_t  trans_pt100[];
extern float    trans_temp[];

#ifdef __cplusplus
} // extern "C"
#endif

#endif // TMET_TABLES_H

//=============================================================================
