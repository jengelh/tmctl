/*=============================================================================
tmctl - Conrad Telemetrics Unit data capturing and analyzation tools
  Copyright © Jan Engelhardt <jengelh [at] linux01 gwdg de>, 2002 - 2005
  -- License restrictions apply (GPL v2)

  This file is part of tmctl.
  tmctl is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; however ONLY version 2 of the License.

  tmctl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program kit; if not, write to:
  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
  Boston, MA  02110-1301  USA

  -- For details, see the file named "LICENSE.GPL2"
=============================================================================*/
#ifndef TMET_DISK_H
#define TMET_DISK_H 1

#include <stdint.h>

#ifdef __GNUC__
#    define _A_PACKED __attribute__((packed))
#else
#    define _A_PACKED
#endif

enum {
    CH_0     = 0,
    CH_BARO  = 1, // was 6?
    CH_HYGRO = 2,
    CH_3     = 3,
    CH_4     = 4,
    CH_LUX   = 5,
    CH_6     = 6,
    CH_TEMP  = 7,
};

struct tframe {
    // Telemet data frame as it is received by the unit
    uint8_t analog[8], digital, fqhi, fqlo;
    // Time passed since Telemetric Unit was connected to power / bootup
    uint8_t min, sec, msec;
} _A_PACKED;

struct logentry {
    int32_t st;   // system time
    int16_t stm;  // system time, milliseconds part
    struct tframe data;
} _A_PACKED;

#endif // TMET_DISK_H

//=============================================================================
